package com.maxim.jms.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.maxim.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {

	private static Logger LOGGER = LogManager.getLogger(ConsumerListener.class.getName());
			
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	ConsumerAdapter consumerAdapter;
	
	public void onMessage(Message message) {
		
		LOGGER.info("In onMessage()");
		String json = null;
		
		if(message instanceof TextMessage){
			
			
			try {
				json = ((TextMessage) message).getText();
				LOGGER.info("Sending JSON to DB" + json);
				consumerAdapter.sendTOMongo(json);
			} catch (Exception e) {
				LOGGER.error("Sending JSON to DB" + json);
				jmsTemplate.convertAndSend(json);
			
			}
		}
	}

	

}
