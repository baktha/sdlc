package com.maxim.jms.adapter;

import static org.junit.Assert.*;

import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConsumerAdapterTest {

	private String json = "{\"menu\": {  \"id\": \"file\",  \"value\": \"File\",  \"popup\": \"Menu item\"}}";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendTOMongo() throws UnknownHostException {
		
		ConsumerAdapter adapter = new ConsumerAdapter();
		
		try {
			adapter.sendTOMongo(json);
			assertNotNull(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
