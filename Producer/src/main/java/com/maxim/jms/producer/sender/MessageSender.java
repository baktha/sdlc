package com.maxim.jms.producer.sender;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

	private static Logger LOGGER  = LogManager.getLogger(MessageSender.class);

	@Autowired
	JmsTemplate template;
	public void send(String json) {
		
		try {
			
			template.convertAndSend(json);
			LOGGER.info("Message sent");
		} catch (JmsException e) {
			
			LOGGER.error("Message : " + json);
			LOGGER.error(e);
		}
		
	} 
	
	
	

}
