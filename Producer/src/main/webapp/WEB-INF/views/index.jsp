<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<body>
	<h2>Vendor Entrty</h2>
	<div id="form">
		<form:form modelAttribute="vendor" action="vendor">
			<fieldset>

				<legend>Vendor Information</legend>
				<label for="vendorName">Vendor Name</label>
				<form:input path="vendorName"/>
				<br/>
				<label for="firstName">First Name</label>
				<form:input path="firstName"/>
				
				<br/>
				<label for="lasteName">Laste Name</label>
				<form:input path="lasteName"/>
				
				<br/>
				<label for="address">Address</label>
				<form:input path="address"/>
				
				<br/>
				<label for="city">City</label>
				<form:input path="city"/>
				
				<br/>
				<label for="state">State</label>
				<form:input path="state"/>
				
				<br/>
				<label for="zipCode">ZipCode</label>
				<form:input path="zipCode"/>
				
				<br/>
				<label for="phoneNumber">Phone Number</label>
				<form:input path="phoneNumber"/>
				
				<br/>
				<label for="email">Email</label>
				<form:input path="email"/>
				
				<br/>
				<input type="submit" value="Submit">

			</fieldset>
		</form:form>
	</div>
</body>
</html>
